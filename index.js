function getUniqueElements(a1, a2) {
	const result = new Set();                       // Используем Set для удаления дубликатов в выдаче
	a2 = a2.reverse();                              // O(n) - реверс с целью использовать pop() вместо shift()
	
	a1.forEach(value => {                           // O(n) - цикл
		if (value < a2[a2.length - 1]) {                // O(1) - операция чтения по индексу
			result.add(value);                          // O(1) - операция добавления
		} else {
			while (a2[a2.length - 1] < value) {         // O(1) - операция чтения по индексу
				a2.pop();                               // O(1) - операция удаления последнего элемента
			}
			if (a2[a2.length - 1] !== value) {          // O(1) - операция чтения по индексу
				result.add(value);                      // O(1) - операция добавления
			}
		}
	});
	return Array.from(result);                      // O(n) - преобразование Set в Array
}

module.exports = getUniqueElements;                 // Итоговая сложность O(n)
